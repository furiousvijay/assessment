import { Link, Navigate, Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import './App.css';
import { createBrowserHistory } from "history";
import SignUpPage from './modules/signup/signup';
import React from 'react';
import Login from './modules/login/login';
import Products from './modules/products/products';
function LoadingPage() {
  return (
    <div className="loader-text">
      <div>Loading...</div>
    </div>
  );
}
const products = [
  {
    name: "product 1",
    description: "lorem ipsum ",
    url: "https://www.littlepearlbooks.com/uploads/productinner_img/learning%20with31136106584.jpg",
  },
  {
    name: "product 2",
    description: "lorem ipsum ",
    url: "https://www.littlepearlbooks.com/uploads/productinner_img/learning%20with2119786333.jpg",
  },
  {
    name: "product 3",
    description: "lorem ipsum ",
    url: "https://www.littlepearlbooks.com/uploads/productinner_img/LearningWith-01558344504.jpg",
  },
  {
    name: "product 4",
    description: "lorem ipsum ",
    url: "https://www.littlepearlbooks.com/uploads/productinner_img/learning%20with41059631946.jpg",
  },
];

function App() {
  return (
    <div className="App">
      <Router history={createBrowserHistory()} basename="/">
        <React.Suspense fallback={<LoadingPage />}>
          <Routes>
            <Route path="/" element={<InitialRoute isLoggedIn={false} />} />
            <Route path="/login" element={<Login />} />
            <Route path="/signup" element={<SignUpPage  />} />
            <Route path="/products" element={<Products items={products} />} />            
            <Route path="*" element={<PageNotFound />} />
          </Routes>
        </React.Suspense>
      </Router>
    </div>
  );
}
const InitialRoute = ({ isLoggedIn }) => {
  return <Navigate to={isLoggedIn ? "/products" : "/login"} />;
};
export const PageNotFound = () => (
  <div className="not-found">
    Page Not Found. <Link to="/login">Click here </Link>&nbsp;to redirect to Loginpage.
  </div>
);
export default App;
