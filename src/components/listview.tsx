import * as React from "react";
import "./listview.css";
interface PropsType<Props> {
  items: Props[];
  renderer: (item: Props) => React.ReactNode;
  filterFn?: (item: Props) => boolean;
}

export default function ListView<Props>( props: PropsType<Props> ) {
  const items = props.filterFn
    ? props.items.filter(props.filterFn)
    : props.items;

  return (
    <div className="list-container">
      {items.map((item, index) => {
        return <React.Fragment key={index}>{props.renderer(item)}</React.Fragment>;
      })}
    </div>
  );
}