export function InputForm(props: any) {
    const { className, title, type, handleChange, errors, valueField, name } =
      props;
    return (
      <div className={className}>
        <label htmlFor={name}>{title}</label>
        <div className="vertical">
          <input id="input-form" type={type} name={name} onChange={handleChange} />
          {errors[valueField].length > 0 && (
            <span style={{ color: "red" }}>{errors[valueField]}</span>
          )}
        </div>
      </div>
    );
  }
  