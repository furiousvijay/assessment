import { Component } from "react";
import { Link } from "react-router-dom";
import { InputForm } from "../../components/inputform";
import "./login.css";
interface Props {
  onSubmit?: any;
}
interface State {
  username: string;
  password: string;
  isValid: boolean;
  errors: any;
}
const defaultValues = {
  username: "",
  password: "",
};

export default class Login extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      ...defaultValues,
      errors: defaultValues,
      isValid: false,
    };
  }
  handleChange = (event: any) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;
    switch (name) {
      case "username":
        errors[name] =
          value.length < 5 ? name + " must be 5 characters long!" : "";
        break;
      case "password":
        errors.password =
          value.length < 8 && value.length > 20
            ? name + " must between 8 to 20 characters"
            : "";
        break;
      default:
        break;
    }
    let obj = Object.assign(this.state, { errors, [name]: value });
    let isValid = this.state.username !== "" && this.state.password !== "";
    if (isValid) {
      Object.values(obj.errors).forEach(
        (val: any) => val.length > 0 && (isValid = false)
      );
    }
    this.setState({ ...obj, isValid });
  };
  render() {
    const { errors } = this.state;
    return (
      <form>
        <h1>Login</h1>
        <InputForm
          name="username"
          type="text"
          className="formitem"
          title="UserName"
          handleChange={this.handleChange}
          errors={errors}
          valueField="username"
        />
        <InputForm
          name="password"
          type="password"
          className="formitem"
          title="Password"
          handleChange={this.handleChange}
          errors={errors}
          valueField="password"
        />
        <div className="login-btn">
          <Link to="/products">
            <button disabled={!this.state.isValid}>submit</button>
          </Link>
          <Link to="/signup">
            <button>signup</button>
          </Link>
        </div>
      </form>
    );
  }
}
