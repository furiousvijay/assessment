import * as React from "react";
import { useState } from "react";
import { Link } from "react-router-dom";
import { InputForm } from "../../components/inputform";
import ListView from "../../components/listview";
import "./products.css";
interface Item {
  name: string;
  description: string;
  url: string;
}

interface PropsType {
  items: Item[];
  renderer: (item: Item) => React.ReactNode;
}

const errors = {
    title:"",
    description:"",
    url:""
}
export default function Products(props: PropsType) {
  const [isAdd, changeAdd] = useState(false);
  function addHandler() {
    changeAdd(false);
  }
  function openAdd() {
    changeAdd(true);
  }
  function handleChange(){

  }
  return (
    <>
      {isAdd ? (
        <div>
          <form>
            <h2 className="product-label">Add Product</h2>
            <InputForm
              name="title"
              type="text"
              className="formitem"
              title="Title"
              errors = {errors}
              handleChange={handleChange}
              valueField="title"
              />
            <InputForm 
              name="description"
              type="text"
              className="formitem"
              title="Description"
              errors = {errors}
              handleChange={handleChange}
              valueField="description"/>
            <InputForm name="image"
              type="text"
              errors = {errors}
              className="formitem"
              title="Image URL"
              handleChange={handleChange}
              valueField="url"/>
          </form>
          <button onClick={addHandler}>Submit</button>
        </div>
      ) : (
        <div className="mainpage">
          <div className="btn-container">
            <button onClick={openAdd} className="add-btn">
              Add
            </button>
            <Link to="/login"><button className="add-btn">Logout</button></Link>
          </div>
          <ListView
            items={props.items}
            filterFn={(item) => item.url !== ""}
            renderer={(item) => <RenderItem item={item} />}
          />
        </div>
      )}
    </>
  );
}

function RenderItem(props: any) {
  const { item } = props;
  return (
    <div className="product-container">
      <img className="product-image" alt="img" src={item.url} />
      <div>
        <div>{item.name}</div>
        <div>{item.description}</div>
      </div>
    </div>
  );
}
