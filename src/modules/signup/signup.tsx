import React from "react";
import { useState } from "react";
import ImageUploading, { ImageListType } from "react-images-uploading";
import { Link } from "react-router-dom";
import { InputForm } from "../../components/inputform";
import OTP from "../../components/otp";
import "./signup.css";
const Regex = RegExp(
  /^\s?[A-Z0–9]+[A-Z0–9._+-]{0,}@[A-Z0–9._+-]+\.[A-Z0–9]{2,4}\s?$/i
);
interface SignUpProps {
  onSubmit?: any;
}
interface SignUpState {
  fullname: string;
  address: string;
  city: string;
  images:any;
  country: string;
  postcode: string;
  email: string;
  username: string;
  password: string;
  isValid: boolean;
  errors: any;
}
const defaultValues = {
  fullname: "",
  address: "",
  city: "",
  country: "",
  postcode: "",
  email: "",
  username: "",
  password: "",
};
export default function SignUpPage() {
  const [isTwoFactor, setTwoFactor] = useState(false);
  function onSignUp(data: object) {
    // data values should send to backend
    console.log("data ", data);
    setTwoFactor(true);
  }
  return isTwoFactor ? <TwoFactor /> : <SignUp onSubmit={onSignUp} />;
}
function TwoFactor() {
  const [otp, setOtp] = useState("");
  const onChange = (value: string) => setOtp(value);

  return (
    <div className="container">
      <h1>Enter OTP</h1>
      <OTP
        autoFocus
        isNumberInput
        length={4}
        className="otpContainer"
        inputClassName="otpInput"
        onChangeOTP={(otp) => onChange(otp)}
      />
      <Link to="/products"><button disabled={!(otp.length === 4)}>Submit</button></Link>
    </div>
  );
}
export class SignUp extends React.Component<SignUpProps, SignUpState> {
  constructor(props: SignUpProps) {
    super(props);
    this.state = {
      ...defaultValues,
      errors: defaultValues,
      isValid: false,
        images:[]
    };
  }
  
  handleSubmit = (e: any) => {
    e.preventDefault();
    this.props.onSubmit(this.state);
  };
  handleChange = (event: any) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;
    switch (name) {
      case "fullname":
      case "address":
      case "postcode":
      case "city":
      case "country":
      case "username":
        errors[name] =
          value.length < 5 ? name + " must be 5 characters long!" : "";
        break;
      case "email":
        errors.email = Regex.test(value) ? "" : "Email is not valid!";
        break;
      case "password":
        errors.password =
          value.length < 8 && value.length > 20
            ? "Password must between 8 to 20 characters"
            : "";
        break;
      default:
        break;
    }
    let obj = Object.assign(this.state, { errors, [name]: value });
    this.setValidation(obj);
  };
  onImgChange = (
    imageList: ImageListType,
    addUpdateIndex: number[] | undefined
  ) => {
    console.log(imageList, addUpdateIndex);
    this.setState({images:imageList as never[]});
  };
  setValidation = (data: SignUpState) => {
    let isValid =
      data.fullname !== "" &&
      data.address !== "" &&
      data.postcode !== "" &&
      data.city !== "" &&
      data.country !== "" &&
      data.email !== "" &&
      data.username !== "" &&
      data.password !== "";
    console.log("isvalid  " + isValid, data);
    if (isValid) {
      Object.values(data.errors).forEach(
        (val: any) => val.length > 0 && (isValid = false)
      );
    }
    this.setState({ ...data, isValid });
  };
  render() {
    const { errors } = this.state;
    return (
      <div className="wrapper">
        <div className="form-wrapper">
          <h2>Sign Up</h2>
          <ImageUploading
              value={this.state.images}
              onChange={this.onImgChange}
              maxNumber={1}
            >
              {({
                imageList,
                onImageUpload,
                onImageRemoveAll,
                onImageUpdate,
                onImageRemove,
                isDragging,
                dragProps,
              }) => (
                // write your building UI
                <div className="upload-image-wrapper">
                  <button className="btn"
                    style={isDragging ? { color: "red" } : undefined}
                    onClick={onImageUpload}
                  >
                    Upload
                  </button>
                  {imageList.map((image, index) => (
                    <div key={index} className="image-item">
                      <img src={image.dataURL} alt="" width="100" />
                      <div className="image-item-btn-wrapper">
                        <button className="btn" onClick={() => onImageUpdate(index)}>
                          Update
                        </button>
                        <button className="btn" onClick={() => onImageRemove(index)}>
                          Remove
                        </button>
                      </div>
                    </div>
                  ))}
                </div>
              )}
            </ImageUploading>
          <form>
            <InputForm
              name="fullname"
              type="text"
              className="formitem"
              title="Full Name"
              handleChange={this.handleChange}
              errors={errors}
              valueField="fullname"
            />
            <InputForm
              name="address"
              type="text"
              className="formitem"
              title="Address"
              handleChange={this.handleChange}
              errors={errors}
              valueField="address"
            />
            <InputForm
              name="postcode"
              type="text"
              className="formitem"
              title="PostCode"
              handleChange={this.handleChange}
              errors={errors}
              valueField="postcode"
            />
            <InputForm
              name="city"
              type="text"
              className="formitem"
              title="City"
              handleChange={this.handleChange}
              errors={errors}
              valueField="city"
            />
            <InputForm
              name="country"
              type="text"
              className="formitem"
              title="Country"
              handleChange={this.handleChange}
              errors={errors}
              valueField="country"
            />
            <InputForm
              name="email"
              type="email"
              className="formitem"
              title="EMail"
              handleChange={this.handleChange}
              errors={errors}
              valueField="email"
            />
            <InputForm
              name="username"
              type="text"
              className="formitem"
              title="Username"
              handleChange={this.handleChange}
              errors={errors}
              valueField="username"
            />
            <InputForm
              name="password"
              type="password"
              className="formitem"
              title="Password"
              handleChange={this.handleChange}
              errors={errors}
              valueField="password"
            />
            
            <div className="signup-btn">
              <button
                onClick={(e) => this.handleSubmit(e)}
                disabled={!this.state.isValid}
              >
                Register
              </button>
              <Link to="/login"><button>
                Back to Login
              </button></Link>
            </div>
          </form>
          
        </div>
      </div>
    );
  }
}
